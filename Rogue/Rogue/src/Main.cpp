#include <cstdint>

// Prevent accidentally selecting integrated GPU
extern "C" {
	__declspec(dllexport) uint32_t AmdPowerXpressRequestHighPerformance = 1;
	__declspec(dllexport) uint32_t NvOptimusEnablement = 1;
}

#include "Engine.h"

int main() {

   Engine::Run();
   return 0;
}
